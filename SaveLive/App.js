import React, { useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { MainScreen, VideosScreen, SettingsScreen } from "./app/screens";
import * as SQLite from "expo-sqlite";
import { Store } from "./app/store";
import { decode, encode } from "base-64";

if (!global.btoa) {
  global.btoa = encode;
}

if (!global.atob) {
  global.atob = decode;
}

const db = SQLite.openDatabase("app.db");
const Tab = createMaterialBottomTabNavigator();

export default function App() {
  return (
    <Store>
      <NavigationContainer>
        <Tab.Navigator
          initialRouteName="Main"
          activeColor="#3F3F3F"
          inactiveColor="#f0edf6"
          barStyle={{ backgroundColor: "#7A7A7A" }}
        >
          <Tab.Screen
            name="Main"
            component={MainScreen}
            options={{
              tabBarLabel: "Main",
              tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons name="home" color={color} size={26} />
              ),
            }}
          />
          <Tab.Screen
            name="Videos"
            component={VideosScreen}
            options={{
              tabBarLabel: "Videos",
              tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons
                  name="library-video"
                  color={color}
                  size={26}
                />
              ),
            }}
          />
          <Tab.Screen
            name="Settings"
            component={SettingsScreen}
            options={{
              tabBarLabel: "Settings",
              tabBarIcon: ({ color }) => (
                <MaterialCommunityIcons
                  name="settings"
                  color={color}
                  size={26}
                />
              ),
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
    </Store>
  );
}
