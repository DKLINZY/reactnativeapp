import React, { createContext, useReducer } from "react";

export const StoreContext = createContext({});

export const UPDATE_SETTING = "UPDATE_SETTING";

const initialState = {
  server_uri: "",
  video_uri: "",
  username: "",
  password: "",
};

const reducer = (state, action) => {
  switch (action.type) {
    case UPDATE_SETTING:
      console.log(`UPDATE_SETTING:${JSON.stringify(action.store)}`);
      return action.store;
    default:
      return state;
  }
};

export const Store = (porps) => {
  const [store, dispatch] = useReducer(reducer, initialState);
  return (
    <StoreContext.Provider value={{ store, dispatch }}>
      {porps.children}
    </StoreContext.Provider>
  );
};
