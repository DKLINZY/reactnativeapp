import * as React from "react";
import { StyleSheet } from "react-native";
import { Header, Button, Text } from "react-native-elements";

export default function VideosHeader({ refreshVideos }) {
  return (
    <Header
      leftComponent={
        <Button
          type="clear"
          icon={{ name: "refresh", color: "white" }}
          onPress={refreshVideos}
        />
      }
      centerComponent={<Text style={styles.headerText}>SaveLive</Text>}
      containerStyle={styles.container}
    />
  );
}

const styles = StyleSheet.create({
  headerText: {
    fontSize: 20,
    color: "#FFFFFF",
  },
  container: {
    backgroundColor: "#7A7A7A",
  },
});
