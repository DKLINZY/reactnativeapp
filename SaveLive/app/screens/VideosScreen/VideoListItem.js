import React from "react";
import { ListItem } from "react-native-elements";

export default function VideoListItem({ videoList, selectVideo }) {
  return (
    <>
      {videoList.map((item, index) => (
        <ListItem
          key={index}
          title={item.name}
          subtitle={`Date: ${item.createAt}    Size: ${item.size}`}
          onPress={() => selectVideo(item.name)}
          bottomDivider
        />
      ))}
    </>
  );
}
