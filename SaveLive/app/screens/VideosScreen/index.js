import React, { useContext, useState, useEffect } from "react";
import { StyleSheet, View, ScrollView } from "react-native";
import VideosHeader from "./VideosHeader";
import VideoPlayer from "./VideoPlayer";
import VideoListItem from "./VideoListItem";
import { StoreContext } from "../../store";

export default function VideosScreen() {
  const { store } = useContext(StoreContext);
  const headers = new Headers({
    Authorization: "Basic " + btoa(`${store.username}:${store.password}`),
  });

  const [videoList, setVideoList] = useState([]);
  const [videoUri, setVideoUri] = useState(
    "http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4"
  );

  useEffect(() => {
    refreshVideos();
  }, []);

  const refreshVideos = () => {
    fetch(`${store.server_uri}/videos`, { headers })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson.videos);
        setVideoList(responseJson.videos);
        // setVideoUri(`${store.video_uri}/${responseJson.videos[0].name}`);
      });
  };

  const selectVideo = (name) => {
    console.log(`${store.video_uri}/${name}`);
    setVideoUri(`${store.video_uri}/${name}`);
  };

  return (
    <>
      <VideosHeader refreshVideos={refreshVideos} />
      <View style={styles.container}>
        <VideoPlayer uri={videoUri} />
        <ScrollView style={styles.videoListView}>
          <VideoListItem videoList={videoList} selectVideo={selectVideo} />
        </ScrollView>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  videoListView: {
    width: "90%",
  },
});
