import React from "react";
import { View, Dimensions } from "react-native";
import { Video } from "expo-av";

const { width } = Dimensions.get("window");

export default function VideoPlayer({ uri }) {
  return (
    <>
      <Video
        source={{
          uri,
        }}
        rate={1.0}
        volume={1.0}
        isMuted={false}
        resizeMode="cover"
        shouldPlay={false}
        isLooping={false}
        useNativeControls
        style={{
          margin: 10,
          width: width * 0.9,
          height: (width / 16) * 9 * 0.9,
        }}
      />
    </>
  );
}
