import React, { useState, useContext } from "react";
import {
  StyleSheet,
  Alert,
  SafeAreaView,
  ScrollView,
  Keyboard,
} from "react-native";
import SettingsHeader from "./SettingsHeader";
import ConfigPaper from "./ConfigPaper";
import DebugPaper from "./DebugPaper";

import * as SQLite from "expo-sqlite";
import { StoreContext, UPDATE_SETTING } from "../../store";

const db = SQLite.openDatabase("app.db");

export default function SettingsScreen() {
  const { store, dispatch } = useContext(StoreContext);
  const [serverUri, setServerUri] = useState(store.server_uri);
  const [videoUri, setVideoUri] = useState(store.video_uri);
  const [username, setUsername] = useState(store.username);
  const [password, setPassword] = useState(store.password);
  // console.log(`SettingScreen | server_uri:${server_uri}`);

  const saveSetting = () => {
    if (serverUri === "") {
      alert("Server Address is empty");
    } else if (username === "") {
      alert("Username is empty");
    } else if (password === "") {
      alert("Password is empty");
    } else {
      db.transaction(
        (tx) => {
          tx.executeSql(`
          INSERT OR REPLACE INTO config (id, name, value) 
          VALUES ((SELECT id from config WHERE name = 'server_uri'), 'server_uri', '${serverUri}'),
                ((SELECT id from config WHERE name = 'video_uri'), 'video_uri', '${videoUri}'),
                ((SELECT id from config WHERE name = 'username'), 'username', '${username}'),
                ((SELECT id from config WHERE name = 'password'), 'password', '${password}');
        `);
        },
        null,
        () => {
          dispatch({
            type: UPDATE_SETTING,
            store: Object.assign({}, store, {
              server_uri: serverUri,
              video_uri: videoUri,
              username: username,
              password: password,
            }),
          });
          alert(`Setting Saved`);
          Keyboard.dismiss();
        }
      );
    }
  };

  const clearData = () => {
    Alert.alert(
      "DROP ALL TABLES",
      "All Tables and Data Will be DELETE.",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => {
            db.transaction(
              (tx) => {
                tx.executeSql("DROP TABLE IF EXISTS platform;");
                tx.executeSql("DROP TABLE IF EXISTS room;");
                tx.executeSql("DROP TABLE IF EXISTS config;");
              },
              null,
              () => {
                console.log("Drop All Tables");
                alert("All Data was Cleaned");
              }
            );
          },
        },
      ],
      { cancelable: false }
    );
  };

  const initData = () => {
    db.transaction(
      (tx) => {
        tx.executeSql(`
          CREATE TABLE IF NOT EXISTS platform (
            id integer PRIMARY KEY AUTOINCREMENT,
            platform_name text NOT NULL,
            platform_url text NOT NULL
          );
        `);
        tx.executeSql(`
          CREATE TABLE IF NOT EXISTS room (
            id integer PRIMARY KEY AUTOINCREMENT,
            platform_id integer NOT NULL,
            room_id text NOT NULL
          );
        `);
        tx.executeSql(`
          CREATE TABLE IF NOT EXISTS config (
            id integer PRIMARY KEY AUTOINCREMENT,
            name text NOT NULL,
            value text NULL
          );
        `);
      },
      null,
      () => {
        console.log("create tables");
        alert("create tables");
      }
    );
  };

  const insertData = () => {
    Alert.alert(
      "Insert Dummy Data",
      "Some Dummy Data will be Insert to Related Tables.",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => {
            db.transaction(
              (tx) => {
                tx.executeSql(`
                  INSERT INTO
                    platform (platform_name, platform_url)
                  VALUES
                    ("Youtube", "https://www.youtube.com/")
                  ;
                `);
                tx.executeSql(`
                  INSERT INTO 
                    room (platform_id, room_id)
                  VALUES 
                    (1, "watch?v=mFO8wH8bZUM"),
                    (1, "watch?v=5qap5aO4i9A")
                  ;
                `);
                tx.executeSql(`
                  INSERT OR REPLACE INTO config (id, name, value) 
                  VALUES ((SELECT id from config WHERE name = 'server_uri'), 'server_uri', 'http://192.168.1.99:5050'),
                        ((SELECT id from config WHERE name = 'username'), 'username', 'admin'),
                        ((SELECT id from config WHERE name = 'password'), 'password', '11241124');
                `);
              },
              null,
              () => {
                console.log("Insert Dummy Data");
              }
            );
          },
        },
      ],
      { cancelable: false }
    );
  };

  const getData = () => {
    db.transaction(
      (tx) => {
        tx.executeSql(
          "SELECT * FROM platform;",
          [],
          (_, { rows: { _array } }) => console.log(_array)
        );
        tx.executeSql("SELECT * FROM room;", [], (_, { rows: { _array } }) =>
          console.log(_array)
        );
        tx.executeSql("SELECT * FROM config;", [], (_, { rows: { _array } }) =>
          console.log(_array)
        );
      },
      null,
      () => {
        console.log("Get Data");
      }
    );
  };

  const getTables = () => {
    db.transaction(
      (tx) => {
        tx.executeSql(
          "SELECT * FROM sqlite_master WHERE type='table';",
          [],
          (_, { rows: { _array } }) => console.log(_array)
        );
      },
      null,
      () => {
        console.log("Get all Tables");
      }
    );
  };

  return (
    <>
      <SettingsHeader saveSetting={saveSetting} />
      <ScrollView style={styles.container}>
        <ConfigPaper
          serverUri={serverUri}
          setServerUri={setServerUri}
          videoUri={videoUri}
          setVideoUri={setVideoUri}
          username={username}
          setUsername={setUsername}
          password={password}
          setPassword={setPassword}
        />
        <DebugPaper
          initData={initData}
          clearData={clearData}
          getTables={getTables}
          insertData={insertData}
          getData={getData}
        />
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    // paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
});
