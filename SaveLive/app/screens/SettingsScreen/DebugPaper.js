import React from "react";
import { StyleSheet, View } from "react-native";
import { Button, Text } from "react-native-elements";

export default function DebugPaper({
  initData,
  clearData,
  getTables,
  insertData,
  getData,
}) {
  return (
    <View style={styles.serverSettingPaper}>
      <Text style={styles.titleText}>Database Debug</Text>
      <Button style={styles.button} title={"Init Tables"} onPress={initData} />
      <Button
        style={styles.button}
        title={"Drop all Tables"}
        onPress={clearData}
      />
      <Button
        style={styles.button}
        title={"Get Tables Info"}
        onPress={getTables}
      />
      <Button
        style={styles.button}
        title={"Insert Dummy Data"}
        onPress={insertData}
      />

      <Button style={styles.button} title={"Get Data"} onPress={getData} />
    </View>
  );
}

const styles = StyleSheet.create({
  serverSettingPaper: {
    marginTop: 15,
    marginHorizontal: 20,
    padding: 10,
    borderRadius: 10,
    backgroundColor: "#F3F3F3",
  },
  titleText: {
    fontSize: 20,
    textAlign: "center",
    marginVertical: 10,
  },
  button: {
    margin: 10,
  },
});
