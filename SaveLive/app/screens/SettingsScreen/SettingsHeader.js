import React from "react";
import { StyleSheet } from "react-native";
import { Header, Button, Text } from "react-native-elements";

export default function SettingsHeader({ saveSetting }) {
  return (
    <Header
      centerComponent={<Text style={styles.headerText}>SaveLive</Text>}
      rightComponent={
        <Button
          type="clear"
          // title="Save"
          icon={{ name: "save", color: "white" }}
          onPress={saveSetting}
        />
      }
      containerStyle={styles.container}
    />
  );
}

const styles = StyleSheet.create({
  headerText: {
    fontSize: 20,
    color: "#FFFFFF",
  },
  container: {
    backgroundColor: "#7A7A7A",
  },
});
