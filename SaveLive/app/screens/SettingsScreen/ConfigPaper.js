import React from "react";
import { StyleSheet, View } from "react-native";
import { Input } from "react-native-elements";

export default function ConfigPaper({
  serverUri,
  setServerUri,
  videoUri,
  setVideoUri,
  username,
  setUsername,
  password,
  setPassword,
}) {
  return (
    <View style={styles.serverSettingPaper}>
      <Input
        label={"API Server Address"}
        value={serverUri}
        onChangeText={(value) => {
          setServerUri(value);
        }}
      />
      <Input
        label={"Video Server Address"}
        value={videoUri}
        onChangeText={(value) => {
          setVideoUri(value);
        }}
      />
      <Input
        label={"Username"}
        value={username}
        onChangeText={(value) => {
          setUsername(value);
        }}
      />
      <Input
        label={"Password"}
        secureTextEntry={true}
        value={password}
        onChangeText={(value) => {
          setPassword(value);
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  serverSettingPaper: {
    marginTop: 15,
    marginHorizontal: 20,
    padding: 10,
    borderRadius: 10,
    backgroundColor: "#F3F3F3",
  },
});
