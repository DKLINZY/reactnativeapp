import React from "react";
import { StyleSheet, View, Switch } from "react-native";
import { Button, Text } from "react-native-elements";
import { MaterialCommunityIcons } from "@expo/vector-icons";

export default function RoomPaper({
  uri,
  data,
  roomStatus,
  recordToggle,
  onlineStatus,
}) {
  return (
    <>
      {data.map((id, index) => {
        return (
          <View key={index + id} style={styles.roomPaper}>
            <View style={styles.roomTextView}>
              <Text style={styles.roomText}>{id}</Text>
            </View>
            <View style={styles.roomRefreshView}>
              <Button
                icon={{ name: "refresh", color: "#737373", size: 26 }}
                type="clear"
                style={styles.roomRefresh}
                loading={roomStatus[id].isLoading}
                onPress={() => {
                  onlineStatus(uri, id);
                }}
              />
            </View>
            <View style={styles.roomIsOnlineView}>
              {roomStatus[id].isOnline ? (
                <MaterialCommunityIcons
                  name="emoticon-happy-outline"
                  color="green"
                  size={30}
                />
              ) : (
                <MaterialCommunityIcons
                  name="emoticon-sad-outline"
                  color="red"
                  size={30}
                />
              )}
            </View>
            <View style={styles.roomSwitchView}>
              <Switch
                trackColor={{ false: "#767577", true: "#75D1FF" }}
                thumbColor={roomStatus[id].isRecording ? "#FFF" : "#FFF"}
                onValueChange={() => {
                  recordToggle(uri, id);
                }}
                value={roomStatus[id].isRecording}
              />
            </View>
          </View>
        );
      })}
    </>
  );
}

const styles = StyleSheet.create({
  roomPaper: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    backgroundColor: "#E0E0E0",
    borderRadius: 10,
    padding: 10,
    margin: 4,
  },
  roomTextView: {
    flex: 1,
  },
  roomText: {
    fontSize: 20,
  },
  roomRefreshView: {
    width: 50,
    margin: 1,
  },
  roomRefresh: {
    flex: 1,
  },
  roomIsOnlineView: {
    width: 50,
    padding: 5,
    margin: 1,
  },
  roomSwitchView: {
    width: 50,
    margin: 2,
  },
});
