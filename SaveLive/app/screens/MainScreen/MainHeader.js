import React from "react";
import { StyleSheet } from "react-native";
import { Header, Text, Button } from "react-native-elements";

export default function MainHeader({ refreshStatus, setAddModalVisible }) {
  return (
    <Header
      statusBarProps={{ translucent: true }}
      leftComponent={
        <Button
          type="clear"
          icon={{ name: "refresh", color: "white" }}
          onPress={refreshStatus}
        />
      }
      centerComponent={<Text style={styles.headerText}>SaveLive</Text>}
      rightComponent={
        <Button
          type="clear"
          icon={{ name: "add", color: "white" }}
          onPress={() => {
            setAddModalVisible(true);
          }}
        />
      }
      containerStyle={styles.container}
    />
  );
}

const styles = StyleSheet.create({
  headerText: {
    fontSize: 20,
    color: "#FFFFFF",
  },
  container: {
    backgroundColor: "#7A7A7A",
  },
});
