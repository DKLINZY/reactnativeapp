import React from "react";
import { StyleSheet, View, Text } from "react-native";
import RoomPaper from "./RoomPaper";

export default function PlatformPaper({
  data,
  roomStatus,
  recordToggle,
  onlineStatus,
}) {
  return (
    <View style={styles.platformPaper}>
      {data.map((platform) => {
        return (
          <View key={platform.platform} style={styles.platform}>
            <Text style={styles.platformPaperTitle}>{platform.platform}</Text>
            <RoomPaper
              uri={platform.uri}
              data={platform.data}
              roomStatus={roomStatus}
              recordToggle={recordToggle}
              onlineStatus={onlineStatus}
            />
          </View>
        );
      })}
    </View>
  );
}

const styles = StyleSheet.create({
  platformPaper: {
    flex: 1,
    marginTop: 10,
    marginHorizontal: 10,
  },
  platformPaperTitle: {
    fontSize: 30,
    marginHorizontal: 10,
    marginVertical: 5,
  },
  platform: {
    backgroundColor: "#F3F3F3",
    borderRadius: 10,
    padding: 10,
    margin: 10,
  },
});
