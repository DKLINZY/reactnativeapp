import React, { useState } from "react";
import { StyleSheet, View, Modal, ScrollView } from "react-native";
import { Button, Input, ListItem, Text } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";

export default function RoomList({
  roomList,
  platformList,
  idModalVisible,
  setIdModalVisible,
  deleteRoom,
  saveToRoomList,
}) {
  const [newRoom, setNewRoom] = useState({
    id: 0,
    platform_id: 0,
    platform_url: "",
    room_id: "",
  });

  const roomModalValueChange = (action, value) => {
    let _newRoom = { ...newRoom };
    switch (action) {
      case "platform":
        _newRoom["platform_id"] = value ? platformList[value - 1].id : 0;
        _newRoom["platform_url"] = value ? platformList[value - 1].value : "";
        break;
      case "room":
        _newRoom["room_id"] = value;
        break;
    }
    setNewRoom(_newRoom);
  };

  const editRoom = (id, platform_id, platform_url, room_id) => {
    setNewRoom({ id, platform_id, platform_url, room_id });
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={idModalVisible}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
      }}
    >
      <View style={styles.container}>
        <View style={styles.modalView}>
          <Button
            style={styles.closeButton}
            type="clear"
            icon={{ name: "close", size: 20 }}
            onPress={() => {
              setIdModalVisible(false);
            }}
          />

          <RNPickerSelect
            items={platformList}
            value={newRoom.platform_url}
            onValueChange={(_, index) => {
              roomModalValueChange("platform", index);
            }}
            style={pickerSelectStyles}
          />

          <Input
            placeholder="e.g. room_id"
            label={"Room ID"}
            value={newRoom.room_id}
            onChangeText={(value) => {
              roomModalValueChange("room", value);
            }}
          />
          <View style={styles.saveView}>
            <Button
              style={styles.modalButton}
              type="clear"
              title="Clear"
              onPress={() =>
                setNewRoom({
                  id: 0,
                  platform_id: 0,
                  platform_url: "",
                  room_id: "",
                })
              }
            />
            <Button
              style={styles.modalButton}
              type="clear"
              title="Save"
              onPress={() => {
                saveToRoomList(newRoom);
                setNewRoom({
                  id: 0,
                  platform_id: 0,
                  platform_url: "",
                  room_id: "",
                });
              }}
            />
          </View>

          <Text style={styles.titleText}>Room</Text>
          <ScrollView style={styles.scrollView}>
            {roomList.map((item, index) => (
              <ListItem
                key={index}
                title={item.room_id}
                subtitle={item.platform_url}
                onPress={() =>
                  editRoom(
                    item.id,
                    item.platform_id,
                    item.platform_url,
                    item.room_id
                  )
                }
                onLongPress={() => {
                  deleteRoom(item.id, item.platform_name);
                  setNewRoom({
                    id: 0,
                    platform_id: 0,
                    platform_url: "",
                    room_id: "",
                  });
                }}
                bottomDivider
              />
            ))}
          </ScrollView>
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    width: "90%",
    height: "80%",
    backgroundColor: "white",
    borderRadius: 20,
    paddingHorizontal: 5,
    paddingBottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalButton: {
    margin: 5,
  },
  closeButton: {
    alignSelf: "flex-end",
  },
  saveView: {
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  titleText: {
    fontSize: 20,
    textAlign: "center",
    marginTop: 30,
  },
  scrollView: {
    height: 300,
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    margin: 10,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: "gray",
    borderRadius: 4,
    color: "black",
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    margin: 10,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: "purple",
    borderRadius: 8,
    color: "black",
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});
