import React, { useState, useEffect, useContext } from "react";
import { StyleSheet, View, ScrollView, Alert, Keyboard } from "react-native";

import MainHeader from "./MainHeader";
import PlatformPaper from "./PlatformPaper";
import OptionList from "./OptionList";
import PlatformList from "./PlatformList";
import RoomList from "./RoomList";

import * as SQLite from "expo-sqlite";
import { StoreContext, UPDATE_SETTING } from "../../store";

const db = SQLite.openDatabase("app.db");

export default function MainScreen() {
  const [addModalVisible, setAddModalVisible] = useState(false);
  const [platformModalVisible, setPlatformModalVisible] = useState(false);
  const [idModalVisible, setIdModalVisible] = useState(false);

  const [platformList, setPlatformList] = useState([]);

  const [roomList, setRoomLlist] = useState([]);

  const [data, setData] = useState([]);
  const [roomStatus, setRoomStatus] = useState({});

  const { store, dispatch } = useContext(StoreContext);

  const headers = new Headers({
    Authorization: "Basic " + btoa(`${store.username}:${store.password}`),
  });

  useEffect(() => {
    db.transaction(
      (tx) => {
        tx.executeSql(`
          CREATE TABLE IF NOT EXISTS platform (
            id integer PRIMARY KEY AUTOINCREMENT,
            platform_name text NOT NULL,
            platform_url text NOT NULL
          );
        `);
        tx.executeSql(`
          CREATE TABLE IF NOT EXISTS room (
            id integer PRIMARY KEY AUTOINCREMENT,
            platform_id integer NOT NULL,
            room_id text NOT NULL
          );
        `);
        tx.executeSql(`
          CREATE TABLE IF NOT EXISTS config (
            id integer PRIMARY KEY AUTOINCREMENT,
            name text NOT NULL,
            value text NOT NULL
          );  
        `);
      },
      () => {
        console.log("init database fail");
      },
      () => {
        console.log("init database success");
        refreshPlatformData();
        getServerUri();
      }
    );
  }, []);

  useEffect(() => {
    let _data = [];
    let _roomStatus = {};

    for (let platform of platformList) {
      const rooms = roomList.filter(({ platform_id }) => {
        return platform.id === platform_id;
      });
      let platform_data = {
        platform: platform.label,
        uri: platform.value,
        data: [],
      };
      for (let room of rooms) {
        platform_data.data.push(room.room_id);
        let status = {};
        status["isOnline"] = false;
        status["isLoading"] = false;
        status["isRecording"] = false;
        _roomStatus[room.room_id] = status;
      }
      _data.push(platform_data);
    }
    console.log(_data);
    setData(_data);
    setRoomStatus(_roomStatus);
  }, [roomList]);

  //functions

  const getServerUri = () => {
    db.transaction(
      (tx) => {
        tx.executeSql(
          `SELECT name, value FROM config`,
          [],
          (_, { rows: { _array } }) => {
            console.log("get config data:", _array);
            if (_array.length > 0) {
              const apiUri = _array.find((item) => {
                return item.name === "server_uri";
              });
              const videoUri = _array.find((item) => {
                return item.name === "video_uri";
              });
              const user = _array.find((item) => {
                return item.name === "username";
              });
              const passwd = _array.find((item) => {
                return item.name === "password";
              });
              dispatch({
                type: UPDATE_SETTING,
                store: Object.assign({}, store, {
                  server_uri: apiUri ? apiUri.value : "",
                  video_uri: videoUri ? videoUri.value : "",
                  username: user ? user.value : "",
                  password: passwd ? passwd.value : "",
                }),
              });
            }
          }
        );
      },
      () => console.log("get server uri fail"),
      () => console.log("get server uri success")
    );
  };

  const refreshPlatformData = () => {
    db.transaction((tx) => {
      //platform list
      tx.executeSql("SELECT * FROM platform", [], (_, { rows: { _array } }) => {
        // console.log(_array);
        let _platformList = [];
        _array.map((platform) => {
          // console.log(platform);
          const _platform = {
            id: platform.id,
            label: platform.platform_name,
            value: platform.platform_url,
          };
          // console.log(JSON.stringify(_platform));
          _platformList.push(_platform);
        });
        setPlatformList(_platformList);
      });
      tx.executeSql(
        "SELECT * FROM platform INNER JOIN room ON platform.id = room.platform_id",
        [],
        //room list
        (_, { rows: { _array } }) => {
          // console.log(_array);
          setRoomLlist(_array);
        }
      );
    });
  };

  const refreshTasks = (tasks) => {
    // console.log("tasks", tasks);
    let _roomStatus = { ...roomStatus };
    for (let task of Object.keys(tasks)) {
      if (_roomStatus[task]) {
        // console.log(task, _roomStatus[task]);
        _roomStatus[task].isRecording = tasks[task];
      }
    }
    setRoomStatus(_roomStatus);
  };

  const onlineStatus = (platform, id) => {
    // console.log(`${store.server_uri}/info?platform=${platform}&id=${id}`);

    let _roomStatus = { ...roomStatus };
    _roomStatus[id].isLoading = true;
    setRoomStatus(_roomStatus);

    fetch(`${store.server_uri}/info?platform=${platform}&id=${id}`, { headers })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        _roomStatus = { ...roomStatus };
        _roomStatus[id].isOnline = responseJson.isActive ? true : false;
        _roomStatus[id].isLoading = false;
        setRoomStatus(_roomStatus);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const refreshStatus = () => {
    console.log("refreshStatus");

    fetch(`${store.server_uri}/tasks`, { headers })
      .then((response) => response.json())
      .then((responseJson) => {
        refreshTasks(responseJson.tasks);
      });

    for (let plfm of data) {
      for (let id of plfm.data) {
        // console.log(plfm.uri, id);
        onlineStatus(plfm.uri, id);
      }
    }
  };

  const recordToggle = (uri, id) => {
    console.log(id);
    let _roomStatus = { ...roomStatus };

    if (_roomStatus[id].isRecording === false) {
      console.log("start");
      fetch(`${store.server_uri}/save?platform=${uri}&id=${id}`, { headers })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          refreshTasks(responseJson.tasks);
          if (responseJson.isActive === true) {
            _roomStatus[id].isRecording = true;
          }
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          setRoomStatus(_roomStatus);
        });
    } else {
      console.log("kill");
      fetch(`${store.server_uri}/kill?platform=${uri}&id=${id}`, { headers })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          refreshTasks(responseJson.tasks);
          _roomStatus[id].isRecording = false;
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
          setRoomStatus(_roomStatus);
        });
    }
  };

  // save/delete platform and room

  const saveToPlatformList = (newPlatform) => {
    if (newPlatform.label === "" || newPlatform.value === "") {
      alert(`Platform Name or Address Can't be Empty`);
    } else {
      // console.log(`label:${newPlatform.label}, value:${newPlatform.value}`);
      let query = "";
      if (newPlatform.id === 0) {
        query = `
          INSERT INTO platform (platform_name, platform_url)
          VALUES ("${newPlatform.label}", "${newPlatform.value}");
        `;
      } else {
        query = `
          UPDATE platform 
          SET platform_name = '${newPlatform.label}', 
              platform_url = '${newPlatform.value}'
          WHERE id = ${newPlatform.id}
        `;
      }
      db.transaction((tx) => {
        tx.executeSql(query, []);
      });
      refreshPlatformData();
      Keyboard.dismiss();
    }
  };

  const deletePlatform = (id, label) => {
    Alert.alert(
      "Delete Platform",
      `Delete Platfrom ${label} and it's Room IDs?`,
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => {
            db.transaction((tx) => {
              tx.executeSql(`DELETE FROM platform WHERE id = ${id}`);
              tx.executeSql(`DELETE FROM room WHERE platform_id = ${id}`);
            });
            refreshPlatformData();
          },
        },
      ],
      { cancelable: false }
    );
  };

  const saveToRoomList = (newRoom) => {
    // console.log(newRoom);
    if (newRoom.platform_id === 0 || newRoom.room_id === "") {
      alert("Platform id or Room id is empty");
    } else {
      let query = "";
      if (newRoom.id === 0) {
        query = `
          INSERT INTO room (platform_id, room_id)
          VALUES ("${newRoom.platform_id}", "${newRoom.room_id}");
        `;
      } else {
        query = `
          UPDATE room
          SET platform_id = '${newRoom.platform_id}',
              room_id = '${newRoom.room_id}'
          WHERE id = ${newRoom.id}
        `;
      }
      // console.log(query);
      db.transaction((tx) => {
        tx.executeSql(query, []);
      });
      refreshPlatformData();
      Keyboard.dismiss();
    }
  };

  const deleteRoom = (id, platform_name) => {
    Alert.alert(
      "Delete Platform",
      `Delete Room ID ${platform_name}?`,
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => {
            db.transaction((tx) => {
              tx.executeSql(`DELETE FROM room WHERE id = ${id}`);
            });
            refreshPlatformData();
          },
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <>
      <MainHeader
        refreshStatus={refreshStatus}
        setAddModalVisible={setAddModalVisible}
      />
      <OptionList
        addModalVisible={addModalVisible}
        setAddModalVisible={setAddModalVisible}
        setPlatformModalVisible={setPlatformModalVisible}
        setIdModalVisible={setIdModalVisible}
      />
      <PlatformList
        platformList={platformList}
        platformModalVisible={platformModalVisible}
        setPlatformModalVisible={setPlatformModalVisible}
        deletePlatform={deletePlatform}
        saveToPlatformList={saveToPlatformList}
      />
      <RoomList
        roomList={roomList}
        platformList={platformList}
        idModalVisible={idModalVisible}
        setIdModalVisible={setIdModalVisible}
        deleteRoom={deleteRoom}
        saveToRoomList={saveToRoomList}
      />

      <View style={styles.container}>
        <ScrollView>
          <PlatformPaper
            data={data}
            roomStatus={roomStatus}
            recordToggle={recordToggle}
            onlineStatus={onlineStatus}
          />
        </ScrollView>
      </View>
    </>
  );
}

//style
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    // paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
});
