import React, { useState } from "react";
import { StyleSheet, Modal, View, ScrollView } from "react-native";
import { Button, Input, Text, ListItem } from "react-native-elements";

export default function PlatformList({
  platformList,
  platformModalVisible,
  setPlatformModalVisible,
  deletePlatform,
  saveToPlatformList,
}) {
  const [newPlatform, setNewPlatform] = useState({
    id: 0,
    label: "",
    value: "",
  });

  const editPlatform = (id, label, value) => {
    setNewPlatform({ id, label, value });
    // console.log(id, label, value);
  };

  const platformModalValueChange = (action, value) => {
    let _newPlatform = { ...newPlatform };
    switch (action) {
      case "label":
        _newPlatform.label = value;
        setNewPlatform(_newPlatform);
        break;
      case "value":
        _newPlatform.value = value;
        setNewPlatform(_newPlatform);
        break;
    }
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={platformModalVisible}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
      }}
    >
      <View style={styles.container}>
        <View style={styles.modalView}>
          <Button
            style={styles.closeButton}
            type="clear"
            icon={{ name: "close", size: 20 }}
            onPress={() => {
              setPlatformModalVisible(false);
            }}
          ></Button>

          <Input
            placeholder="e.g. youtube"
            label={"Platform Name"}
            value={newPlatform.label}
            onChangeText={(value) => {
              platformModalValueChange("label", value);
            }}
          />

          <Input
            placeholder="e.g. www.example.com"
            label={"Address"}
            value={newPlatform.value}
            onChangeText={(value) => {
              platformModalValueChange("value", value);
            }}
          />
          <View style={styles.saveView}>
            <Button
              style={styles.modalButton}
              type="clear"
              title="Clear"
              onPress={() =>
                setNewPlatform({
                  id: 0,
                  label: "",
                  value: "",
                })
              }
            />
            <Button
              style={styles.modalButton}
              type="clear"
              title="Save"
              onPress={() => {
                saveToPlatformList(newPlatform);
                setNewPlatform({ id: 0, label: "", value: "" });
              }}
            />
          </View>

          <Text style={styles.listTitle}>Platform</Text>
          <ScrollView style={styles.scrollView}>
            {platformList.map((item, index) => (
              <ListItem
                key={index}
                title={item.label}
                subtitle={item.value}
                bottomDivider
                onPress={() => {
                  editPlatform(item.id, item.label, item.value);
                }}
                onLongPress={() => {
                  deletePlatform(item.id, item.label);
                  setNewPlatform({ id: 0, label: "", value: "" });
                }}
              />
            ))}
          </ScrollView>
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  modalView: {
    width: "90%",
    height: "80%",
    backgroundColor: "white",
    borderRadius: 20,
    paddingHorizontal: 5,
    paddingBottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalButton: {
    margin: 5,
    width: 300,
  },
  closeButton: {
    alignSelf: "flex-end",
  },
  saveView: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  listTitle: {
    fontSize: 20,
    textAlign: "center",
    marginTop: 30,
  },
  scrollView: {
    height: 300,
  },
});
