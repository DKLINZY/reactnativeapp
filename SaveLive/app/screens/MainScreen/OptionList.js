import React from "react";
import { StyleSheet, Modal, View } from "react-native";
import { Button } from "react-native-elements";

export default function OptionList({
  addModalVisible,
  setAddModalVisible,
  setPlatformModalVisible,
  setIdModalVisible,
}) {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={addModalVisible}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
      }}
    >
      <View style={styles.container}>
        <View style={{ ...styles.modalView, width: 250 }}>
          <Button
            style={styles.closeButton}
            type="clear"
            icon={{ name: "close", size: 20 }}
            onPress={() => {
              setAddModalVisible(false);
            }}
          />
          <Button
            style={styles.modalButton}
            type="clear"
            title="Platform"
            onPress={() => {
              setAddModalVisible(false);
              setPlatformModalVisible(true);
            }}
          />
          <Button
            style={styles.modalButton}
            type="clear"
            title="Room ID"
            onPress={() => {
              setAddModalVisible(false);
              setIdModalVisible(true);
            }}
          />
        </View>
      </View>
    </Modal>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  closeButton: {
    alignSelf: "flex-end",
  },
  modalView: {
    backgroundColor: "white",
    borderRadius: 20,
    paddingHorizontal: 5,
    paddingBottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalButton: {
    margin: 5,
  },
});
